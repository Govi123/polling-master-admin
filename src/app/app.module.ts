import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { LoginComponent } from './login/login.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';

import { HomeComponent } from './home/home.component';
import { MatMenuModule}  from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule,MatIconModule, MatListModule,MatGridListModule, } from '@angular/material';
import {MatSelectModule} from  '@angular/material/select';                              '@angular/material/prebuilt-themes'
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { SidenavComponent } from './sidenav/sidenav.component';
import {MatSortModule} from '@angular/material/sort';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import * as firebase from 'firebase';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { MatSlideToggleModule } from '@angular/material';


export const firebaseConfig = {
  apiKey: "AIzaSyARJ7NFU0blay_Iq4WVVFCltiPUvu3aXJE",
  authDomain: "polling-c830b.firebaseapp.com",
  databaseURL: "https://polling-c830b.firebaseio.com",
  projectId: "polling-c830b",
  storageBucket: "polling-c830b.appspot.com",
  messagingSenderId: "956047222084",
  appId: "1:956047222084:web:358f2255aacb3a87"
};
firebase.initializeApp(firebaseConfig);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SidenavComponent,
    LoadingSpinnerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatTableModule,
    HttpClientModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
   MatIconModule,
   MatListModule,
  MatSelectModule,
  MatGridListModule,
  BrowserAnimationsModule,
  MatSortModule,
  MatSlideToggleModule
  
  
    
     
   
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
