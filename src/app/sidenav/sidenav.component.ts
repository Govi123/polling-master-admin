import { Component, OnInit } from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, OnDestroy} from '@angular/core';
import { Router } from '@angular/router'
import { Location } from '@angular/common';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  path = '';
  showSidenav=true;
  mobileQuery: MediaQueryList;

  fillerNav =[
  {name:"Polls", route:"/home", icon: "person_pin" },
  ];
  
  private _mobileQueryListener: () => void;

  constructor(private router: Router, private location: Location, changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    
    this.router.events.subscribe((val) => {
      this.path = this.location.path();
      if(this.path=='/login'){
        this.showSidenav=false;
      }
      else
        this.showSidenav=true;
    });

    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() {
  }

}
