import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import * as firebase from 'firebase';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	email;
	password;
  data= firebase.database().ref('polls');


  constructor(public router: Router, public http: HttpClient) {

    this.data.on('value', (data)=>{
      console.log(data.val());
    });

   }

  ngOnInit() {
  }
  Login() {
    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    }

    const requestOptions = {                                                                                                                                                                                 
      headers: new HttpHeaders(headerDict), 
    };

    this.http.get('http://asantechcube.com/dream-admin/login.php?email='+this.email+'&password='+this.password).subscribe((result)=>{
      if(result){
        this.router.navigateByUrl('/home');
      }
    });

  }

}
