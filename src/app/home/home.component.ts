import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

import { Router } from '@angular/router';
import {MatTableModule} from '@angular/material/table';
import { HttpClient  } from '@angular/common/http';
import * as firebase from 'firebase';
import { LoadingSpinnerComponent } from './../loading-spinner/loading-spinner.component';
import { FormsModule } from '@angular/forms';





export interface PeriodicElement {
  pollId: string;
  pollTitle: number;
  userId: string;
  date: string;
  time: string;
  status: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit { 
	 displayedColumns: string[] = ['pollId', 'pollTitle', 'status', 'date','time','userId',];
    dataSource=[] ;
    allPolls = firebase.database().ref('polls');

    showSpinner;
    myvalue=false;


  constructor(public form: FormsModule, public http:HttpClient,public router: Router) { 
  console.log(this.myvalue);
   // this.http.get('http://asantechcube.com/dream-admin/getexperts.php').subscribe(data=>{
   //  this.dataSource=data;
   // })
    }
  ngOnInit() {
    this.showSpinner =true;
    this.allPolls.on('value',(data)=>{
     this.dataSource = Object.values(data.val());
     this.showSpinner = false;
      });
 
  }

  toggleStatus(status ,pollId){

    if(status === 'approved'){
      console.log('disapproved');
      this.allPolls.orderByChild('pollId').equalTo(pollId).once('value' ,(data)=>{
       data.forEach((snapshot) =>{
         snapshot.ref.update({ status : 'disapproved'});
       });
      });
    }
    if(status === 'disapproved' || status === 'pending'){
      console.log('approved');
      this.allPolls.orderByChild('pollId').equalTo(pollId).once('value' ,(data)=>{
       data.forEach((snapshot) =>{
         snapshot.ref.update({ status : 'approved'});
       });
      });
    }
  }

}
